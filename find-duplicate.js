function getDuplicate(dataArray) {
    if ( !Array.isArray(dataArray) ) {
        throw('Input data is not an array');
    }

    if (dataArray.length % 2 === 0) {
        throw('Array size is not correct');
    }

    var duplicateValue = false;

    var left = 0;
    var right = dataArray.length - 1;
    var mid = 0;

    while (left < dataArray.length-2) {
        mid = left + Math.floor( (right - left) / 2);

        if (dataArray[mid] == mid + 1) {
            left = mid;
        } else {
            if (dataArray[mid] == dataArray[mid-1]) {
                duplicateValue = dataArray[mid];
                break;
            } else {
                right = mid
            }
        }
    }

    if (dataArray[mid] == dataArray[mid+1]) {
        duplicateValue = dataArray[mid];
    }

    if ( !duplicateValue ) {
        throw('There is no duplicate in the array');
    }

    return duplicateValue;
}